﻿using Modbus.Device;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace test_nmodbus4_000
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            dt2.Interval = TimeSpan.FromMilliseconds(300);
            dt2.Tick += Dt_Tick2;
            dt2.Start();

            this.DataContext = t1;
        }
        DispatcherTimer dt2 = new DispatcherTimer();
        ModbusIpMaster master;
        TcpClient client;
        private async void Dt_Tick2(object sender, EventArgs e)
        {

            if (client == null || client.Connected == false) { tbTcpStatus.Text = "disconn"; wpEnableIfConnected.IsEnabled = false; return; }
            wpEnableIfConnected.IsEnabled = true;

            ushort[] statuses = await master.ReadHoldingRegistersAsync(pAmcStatus_41_002, 1);
            cbAmcMovementComplete.IsChecked = Utilz.GetBit(statuses[0], 2);
            cbAmcProfileComplete.IsChecked = Utilz.GetBit(statuses[0], 3);
            cbAmcFollowingError.IsChecked = Utilz.GetBit(statuses[0], 4);
            cbAmcAtSpeed.IsChecked = Utilz.GetBit(statuses[0], 5);
            cbAmcHomeComplete.IsChecked = Utilz.GetBit(statuses[0], 8);
            cbAmcSoftLim_Neg.IsChecked = Utilz.GetBit(statuses[0], 12);
            cbAmcSoftLim_Pos.IsChecked = Utilz.GetBit(statuses[0], 13);
            cbAmcHomeRequired.IsChecked = Utilz.GetBit(statuses[0], 14);
            cbAmcPositionFeedbackInitialized.IsChecked = Utilz.GetBit(statuses[0], 15);

            ushort[] status2 = await master.ReadHoldingRegistersAsync(pStatusWord_10_040, 1);
            cbAmcFollowingError.IsChecked = Utilz.GetBit(statuses[0], 4);

            ushort[] tmpInh = await master.ReadHoldingRegistersAsync(pSto_Input01_State_08_009, 1);
            cbInhibit.IsChecked = tmpInh[0] == 0;


            ushort[] cvRpm = await master.ReadHoldingRegistersAsync(pFinalSpeedRef_03_001, 2);
            t1.Cv_Rpm = unchecked((short)cvRpm[1]) / 10.0;
            tbCV_RPM.Text = string.Format("{0,6:0.0} RPM", t1.Cv_Rpm);

            ushort[] cvHz = await master.ReadHoldingRegistersAsync(pOutputFreq_05_001, 2);
            t1.Cv_Hz = unchecked((short)cvHz[1]) / 10.0;
            tbCV_Hz.Text = string.Format("{0,6:0.0} Hz", t1.Cv_Hz);

            ushort[] AmcCv = await master.ReadHoldingRegistersAsync(pAmcSpeed_33_005, 2);
            ushort[] AmcCp = await master.ReadHoldingRegistersAsync(pAmcPos_33_004, 2);
            int tmpCP = (((int)AmcCp[0]) << 16) | (int)AmcCp[1];
            tb_cp.Text = string.Format("{0,15:0.0} ", tmpCP);

            ushort[] amp = await master.ReadHoldingRegistersAsync(pCurrentMagnitude_04_001, 2);
            tbImot_A.Text = string.Format("{0,6:0.0} A", amp[1] / 1000.0);

            var tempSpeed = (int)(100 * sldMV.Value);
            var tempSpeed2 = (int)Math.Abs(100 * sldMV.Value);
            var tSpeedRef = new ushort[] { (ushort)(tempSpeed >> 16), (ushort)(tempSpeed & 0x0000FFFF) };
            var tSpeedRef2 = new ushort[] { (ushort)(tempSpeed2 >> 16), (ushort)(tempSpeed2 & 0x0000FFFF) };
            master.WriteMultipleRegisters(pAmcSpeedRef_34_006, tSpeedRef); //amc sv mv
            master.WriteMultipleRegisters(pAmcProfileMaxSpeed_38_003, tSpeedRef2); //amc sv mv

            var tempSP = (int.Parse(tb_sp.Text));
            var tSetP = new ushort[] { (ushort)(tempSP >> 16), (ushort)(tempSP & 0x0000FFFF) };
            master.WriteMultipleRegisters(pAmcPosRef_34_003, tSetP); //amc sv mv

            var hpos = int.Parse(tb_homePos.Text);
            var tHomePos = new ushort[] { (ushort)(hpos >> 16), (ushort)(hpos & 0x0000FFFF) };
            master.WriteMultipleRegisters(pAmcHomePos_40_004, tHomePos); //amc home pos

            tbTcpStatus.Text = client.Connected ? "connected" : "disconn";


            if (_AmcActive)
            {

                // master.WriteSingleRegister(pRun_06_034, (ushort)(pb_mpos.IsChecked == true || pb_mneg.IsChecked == true ? 1 : 0));
                //master.WriteSingleRegister(pRunFw_06_030, (ushort)(pb_mpos.IsChecked == true ? 1 : 0));
                //master.WriteSingleRegister(pRunRev_06_032, (ushort)(pb_mneg.IsChecked == true ? 1 : 0));
                if (pb_mpos.IsChecked == true || pb_mneg.IsChecked == true)
                {
                    master.WriteSingleRegister(pAmcRefSelect_34_007, (ushort)AmcRef.Speed2);
                }
                else
                {
                    master.WriteSingleRegister(pAmcRefSelect_34_007, (ushort)AmcRef.stop0);
                }
            }

            if (cb41_025_EnabeSoftLim.IsChecked == true)
            {
                master.WriteSingleRegister(pAmcSoftlimEnable_41_025, (ushort)(cb41_025_EnabeSoftLim.IsChecked == true ? 1 : 0)); //amc speed ref (mv)
                ushort[] limP = new ushort[] { 33, 33 };
                ushort[] limN = new ushort[] { 44, 44 };
                master.WriteMultipleRegisters(pAmcSoftlimPos_41_026, limP);
                master.WriteMultipleRegisters(pAmcSoftlimNeg_41_027, limN);
                //master.WriteSingleRegister(pAmcSoftlimPos_41_026, ushort.Parse(tbSoftLimPos.Text)); //lim pos
                //master.WriteSingleRegister(pAmcSoftlimNeg_41_027, ushort.Parse(tbSoftLimNeg.Text)); //lim neg
            }
        }

        bool _AmcActive;
        private void pbManStart_Click(object sender, RoutedEventArgs e)
        {
            //master.WriteSingleRegister(pTimingOptionsSelect_02_050, 8); //02.050 timing options select
            //master.WriteSingleRegister(pControlWordEnable_06_043, 1); //02.050 control word enable


            master.WriteSingleRegister(pDriveEnable_06_015, 1); //drive enable
            master.WriteSingleRegister(pAmcSelect_31_001, 1); //amc select
            master.WriteSingleRegister(pAmcEnable_41_001, 1); //amc enable
            master.WriteSingleRegister(pAmcRefSelect_34_007, (ushort)AmcRef.stop0); //amc ref select = stop
            master.WriteSingleRegister(pRun_06_034, (ushort)(1));
            // master.WriteSingleRegister(pAmcSpeedRef_34_006, (ushort)sldMV.Value); //amc speed ref (mv)
            // master.WriteSingleRegister(pAmcSpeedRef_34_006, (ushort)0); //amc speed ref (mv)
            _AmcActive = true;
        }
        //private void pbManEnd_Click(object sender, MouseButtonEventArgs e)
        //{

        //}

        private void pbManEnd_Click(object sender, RoutedEventArgs e)
        {
            master.WriteSingleRegister(pRun_06_034, (ushort)(0));

            master.WriteSingleRegister(pAmcRefSelect_34_007, (ushort)AmcRef.stop0); //amc ref select = stop
            master.WriteSingleRegister(pAmcEnable_41_001, 0); //amc enable 
            master.WriteSingleRegister(pAmcSelect_31_001, 0); //amc select
            //master.WriteSingleRegister(pDriveEnable_06_015, 0); //drive enable BLOKADA PREKO KY3!!!!

            //master.WriteSingleRegister(pTimingOptionsSelect_02_050, 1); //02.050 timing options select
            //master.WriteSingleRegister(pControlWordEnable_06_043, 0); //02.050 control word enable
            _AmcActive = false;
        }
        private void pb_manStop_Click(object sender, MouseButtonEventArgs e)
        {
            master.WriteSingleRegister(pAmcRefSelect_34_007, (ushort)AmcRef.stop0); //amc ref select = stop
        }


        //man+ i man- nece da rade ako je nasetovan na RunFw neki digin!!!!! todo kako da se napravi da budu oba
        private void pbManUp_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) { }
        //   private void pbManUp_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) { master.WriteSingleRegister(pAmcRefSelect_34_007, (ushort)AmcRef.stop0); }
        private void pbManUp_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) { }
        private void pbManDown_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) { }
        private void pbManDown_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) { }

        //private void pbManUp_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) { master.WriteSingleRegister(pRunFw_06_030, 1); }
        //private void pbManUp_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) { master.WriteSingleRegister(pRunFw_06_030, 0); }
        //private void pbManDown_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) { master.WriteSingleRegister(pRunRev_06_032, 1); }
        //private void pbManDown_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) { master.WriteSingleRegister(pRunRev_06_032, 0); }

        //private void pbManUp_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) { master.WriteSingleRegister(pControlWord_06_042, 3); }
        //private void pbManUp_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) { master.WriteSingleRegister(pControlWord_06_042, 1); }
        //private void pbManDown_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) { master.WriteSingleRegister(pControlWord_06_042, 9); }
        //private void pbManDown_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) { master.WriteSingleRegister(pControlWord_06_042, 1); }



        private void click_pb_auto(object sender, MouseButtonEventArgs e)
        {
            master.WriteSingleRegister(pDriveEnable_06_015, 1); //drive enable
            master.WriteSingleRegister(pAmcSelect_31_001, 1); //amc select
            master.WriteSingleRegister(pAmcEnable_41_001, 1); //amc enable

            master.WriteSingleRegister(pRun_06_034, (ushort)(1));

            //var tempSV = (int)sldMV.Value * 100;
            //var tSetV = new ushort[] { (ushort)(tempSV >> 16), (ushort)(tempSV & 0x0000FFFF) };
            //master.WriteMultipleRegisters(pAmcProfileMaxSpeed_38_003, tSetV); //amc sv mv

            master.WriteSingleRegister(pAmcRefSelect_34_007, (ushort)AmcRef.PositionAbs1); //amc ref select = speed
                                                                                           // master.WriteSingleRegister(pAmcSpeedRef_34_006, (ushort)sldMV.Value); //amc speed ref (mv)
        }

        private void click_pb_stop(object sender, MouseButtonEventArgs e)
        {
            master.WriteSingleRegister(pAmcRefSelect_34_007, (ushort)AmcRef.stop0); //amc ref select = speed
            master.WriteSingleRegister(pRun_06_034, (ushort)(0));
        }

       

        private void click_pb_reset(object sender, MouseButtonEventArgs e)
        {
            master.WriteSingleRegister(pDriveReset_10_033, 1); //amc ref select = speed
        }
        private void pb_reset_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            master.WriteSingleRegister(pDriveReset_10_033, 0); //amc ref select = speed
        }

        private void pb_stop_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            master.WriteSingleRegister(pAmcRefSelect_34_007, (ushort)AmcRef.stop0); //amc ref select = speed
            master.WriteSingleRegister(pAmcEnable_41_001, 0); //amc enable
            master.WriteSingleRegister(pAmcSelect_31_001, 0); //amc select
            master.WriteSingleRegister(pDriveEnable_06_015, 0); //drive enable
        }
        private void click_pb_tripSet(object sender, MouseButtonEventArgs e) { master.WriteSingleRegister(pTripSet_10_032, 1); }

        private void pb_tripSet_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e) { master.WriteSingleRegister(pTripSet_10_032, 0); }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
          
            cmbIpAddress.Items.Add("192.168.40.78");           
            cmbIpAddress.SelectedIndex = 0;
        }

        private void pbConnect_Click(object sender, RoutedEventArgs e)
        {
            if (cmbIpAddress.SelectedItem == null) return;

            IPAddress ipAdrTemp;
            if (IPAddress.TryParse(cmbIpAddress.SelectedItem.ToString(), out ipAdrTemp))
            {
                try
                {

                    client = new TcpClient(cmbIpAddress.SelectedItem.ToString(), 502);
                    master = ModbusIpMaster.CreateIp(client);
                }
                catch { MessageBox.Show(string.Format("Unable to connect to {0}", cmbIpAddress.SelectedItem.ToString())); }
            }
        }

        private void pbDisconnect_Click(object sender, RoutedEventArgs e)
        {
            if (client == null) return;
            client.Close();
        }


        public enum AmcRef { stop0 = 0, PositionAbs1 = 1, Speed2 = 2, Home5 = 5 }

        public tst1 t1 = new tst1();
        public class tst1 : INotifyPropertyChanged
        {
            public ushort AmcStatus { get; set; }
            public ushort AmcCommand { get; private set; }
            public int AmcSp { get; set; }
            public int AmcSv { get; set; }
            public double AmcCp { get; set; }
            public double AmcCv { get; set; }
            public double Cv_Rpm { get; set; }
            public double Cv_Hz { get; set; }
            public bool AmcMovementComplete { get { return Utilz.GetBit(AmcStatus, 2); } }
            public bool AmcProfileComplete { get { return Utilz.GetBit(AmcStatus, 3); } }
            public bool AmcFollowingError { get { return Utilz.GetBit(AmcStatus, 4); } }
            public bool AmcSoftLim_Pos { get { return Utilz.GetBit(AmcStatus, 12); } }
            public bool AmcSoftLim_Neg { get { return Utilz.GetBit(AmcStatus, 13); } }
            public bool AmcHomeRequired { get { return Utilz.GetBit(AmcStatus, 2); } }

            protected void NotifyPropertyChanged(String propertyName) { if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName)); }

            public event PropertyChangedEventHandler PropertyChanged;
        }


        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (client != null) client.Close();
        }

        private void pbManStart_Click(object sender, MouseButtonEventArgs e)
        {
            master.WriteSingleRegister(pRun_06_034, (ushort)129); //
        }

        private void pbManEnd_Click(object sender, MouseButtonEventArgs e)
        {

        }

        private void ManNeg_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TimedAction.ExecuteWithDelay(new Action(delegate
            {
                master.WriteSingleRegister(pControlWord_06_042, (ushort)137); //
            }), TimeSpan.FromMilliseconds(100));
        }

        private void ManNeg_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            master.WriteSingleRegister(pControlWord_06_042, (ushort)129); //
        }

        private void ManPos_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            master.WriteSingleRegister(pAmcSelect_31_001, (ushort)0); //
           // master.WriteSingleRegister(pControlWord_06_042, (ushort)386); //
            TimedAction.ExecuteWithDelay(new Action(delegate
            {
                master.WriteSingleRegister(pControlWord_06_042, (ushort)131); //
            }), TimeSpan.FromMilliseconds(100));

        }

        private void ManPos_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            
            master.WriteSingleRegister(pControlWord_06_042, (ushort)129); //
        }

        private void click_pb_home(object sender, MouseButtonEventArgs e)
        {

            master.WriteSingleRegister(pAmcHomeSwitch_40_012, (ushort)0); //
            master.WriteSingleRegister(pAmcRefSelect_34_007, (ushort)AmcRef.Home5); //amc ref select = speed
            master.WriteSingleRegister(pAmcHomeSwitch_40_012, (ushort)1); //
        }

        private void Pb_home_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            master.WriteSingleRegister(pAmcHomeSwitch_40_012, (ushort)0); //
            master.WriteSingleRegister(pAmcRefSelect_34_007, (ushort)AmcRef.stop0); //amc ref select = speed
           
        }
    }
}
